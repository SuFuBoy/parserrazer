import got from 'got';
import saver from './saver.js';

let regExpNameAndImg = /image_as9wS"><img src="([^"]+)" alt="([^"]+)/g;
// let regExpPrice = /card-type-1__price"><span itemprop="price">([^<]+)/g


let testReg;

async function getPage() {
    const data = await got('https://forklifts24.ru/catalog/dla-gruzovikov-i-avtobusov');
    // console.log(typeof data.body);
    let result = data.body.match(regExpNameAndImg).map((group) => {
        let parsedGroup =  group.replace(regExpNameAndImg,'$1 $2').split(' ');
        let image = parsedGroup[0]
        let article = parsedGroup[parsedGroup.length - 1];
        let creater = parsedGroup[parsedGroup.length - 2];
        let name = parsedGroup.slice(1,-2).join(' ').replace(/&quot;/g, '"')
       
        // console.log(group)
       
        // console.log(image,article,creater,name);
        return {image,article,creater,name}
    });
    // var result = regExpNameAndImg.exec(data.body);
    console.log(Object.keys(result[0]).map((key) => {
        return {id: key , title: key.toUpperCase()}
        
    }));
    saver(result,'/csv.csv');
};

getPage();
