import {createObjectCsvWriter as createCsvWriter} from 'csv-writer'


function saver (arrToSave,path) {
    const csvWriter = createCsvWriter({
    path,
    header: Object.keys(arrToSave[0]).map((key) => {
        return {id: key , title: key.toUpperCase()}
        
    }),
    fieldDelimiter: ';',
    // [
    //     {id: 'name', title: 'NAME'},
    //     {id: 'lang', title: 'LANGUAGE'}
    // ]
});
 

 
csvWriter.writeRecords(arrToSave)       // returns a promise
    .then(() => {
        console.log('...Done');
    });
}
export default saver